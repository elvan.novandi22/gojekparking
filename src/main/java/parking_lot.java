import java.io.*;
import java.util.Scanner;

public class parking_lot {
    private static String[] PerintahInit;
    private static String[] RegisNum;
    private static String[] Color;

    public static String[] getPerintahInit() {
        return PerintahInit;
    }

    public static String[] getRegisNum() {
        return RegisNum;
    }

    public static String[] getColor() {
        return Color;
    }

    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        String perintahawal[]= sc.nextLine().split(" ");
        if (perintahawal.length>1){
            String file= perintahawal[1];
            sc= new Scanner(new FileReader(perintahawal[1]));
        }
        String[] perintah= sc.nextLine().split(" ");
        if (perintah[0].equalsIgnoreCase("create_parking_lot")){

            String mulai= initParking(Integer.parseInt(perintah[1]));
            System.out.println(mulai);
            while (sc.hasNext()){
                String[] Bebas= sc.nextLine().split(" ");
                if (Bebas.length>1){
                    if(Bebas[0].equalsIgnoreCase
                            ("leave")){
                        String tinggalkan= Tinggalkan(Integer.parseInt(Bebas[1]));
                        System.out.println(tinggalkan);
                    }
                    else if (Bebas[0].equalsIgnoreCase
                            ("park")){
                        String parkir= Park(Bebas[1],Bebas[2]);
                        System.out.println(parkir);
                    }
                    else if(Bebas[0].equalsIgnoreCase
                            ("registration_numbers_for_cars_with_colour")){
                        String output= cariRegistNumColor(Bebas[1]) ;
                        System.out.println(output);
                    }
                    else if (Bebas[0].equalsIgnoreCase
                            ("slot_numbers_for_cars_with_colour")){
                        String output= cariSlotDenganColor(Bebas[1]);
                        System.out.println(output);
                    }
                    else if (Bebas[0].equalsIgnoreCase
                            ("slot_number_for_registration_number")){
                        String output= SlotForRegisNum(Bebas[1]);
                        System.out.println(output);
                    }
                }
                else if (Bebas.length==1){
                    if (Bebas[0].equalsIgnoreCase("status")){
                        String status= CheckStats();
                        System.out.println(status);
                    }
                }
            }
        }

    }

    static String initParking(int i){
        PerintahInit= new String[i];
        RegisNum= new String[i];
        Color= new String[i];
        return "Created a parking lot with "+i+" slots";
    }
    static String Park( String Regis, String warna){
        boolean kosong=false;
        int slot=0;
        for (int cari=0;cari<PerintahInit.length;cari++){
            if (RegisNum[cari]==null){
                RegisNum[cari]= Regis;
                Color[cari]= warna;
                kosong=true;
                slot=cari+1;
                break;
            }
        }
        if (kosong){
            return "Allocated slot number: "+(slot);
        }
        else {
            return "Sorry, parking lot is full";
        }
    }

    static String Tinggalkan(int leftSlot){
        RegisNum[leftSlot-1]=null;
        Color[leftSlot-1]=null;
        return "Slot number "+leftSlot+" is free";
    }

    static String cariRegistNumColor(String warna){
        String output="";
        boolean ketemu= false;
        for (int q=0;q<Color.length;q++){
            if (Color[q]!=null && Color[q].equalsIgnoreCase(warna)){
                output+=RegisNum[q]+", ";
                ketemu=true;
            }
        }
        output.replaceAll(" ",", ");
        if (ketemu){
            output= output.substring(0, output.length()-2);
            return output;
        }
        else {
            return "Not found";
        }
    }

    static String cariSlotDenganColor(String warna){
        String output="";
        boolean ketemu=false;
        for (int q=0;q<Color.length;q++){
            if (Color[q]!=null && Color[q].equalsIgnoreCase(warna)){
                output=output+(q+1)+", ";
                ketemu=true;
            }
        }
        output.replaceAll(" ",", ");
        if (ketemu){
            output= output.substring(0, output.length()-2);
            return output;
        }
        else {
            return "Not found";
        }
    }

    static String SlotForRegisNum(String Regis){
        int slot=0;
        boolean ketemu=false;
        for (int b=0;b<RegisNum.length;b++){
            if (RegisNum[b]!=null && RegisNum[b].equalsIgnoreCase(Regis)){
                slot=b+1;
                ketemu=true;
                break;
            }
        }
        if (ketemu){
            return Integer.toString(slot);
        }
        else {
            return"Not found";
        }
    }
    static String CheckStats(){
        String output="Slot No. Registration No Colour\n";
        for (int f=0;f<RegisNum.length;f++){
            if (RegisNum[f]!=null){
                output+=(f+1)+" "+RegisNum[f]+" "+Color[f]+"\n";
            }
        }
        output= output.substring(0, output.length()-1);
        return output;
    }
}
