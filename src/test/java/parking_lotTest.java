import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class parking_lotTest {
    @Test
    void initParkingTest() {
        String input= "create_parking_lot 6";
        String[] perintah= input.split(" ");
        parking_lot parking= new parking_lot();
        String output= parking.initParking(Integer.parseInt(perintah[1]));
        assertEquals("Created a parking lot with 6 slots",output);
    }

    @Test
    void parkBisa() {
        String input= "create_parking_lot 6";
        String[] perintah= input.split(" ");
        parking_lot parking= new parking_lot();
        String output= parking.initParking(Integer.parseInt(perintah[1]));
        assertEquals("Created a parking lot with 6 slots",output);
        String inputPark= "park KA-01-HH-1234 White";
        String[] perintahpark= inputPark.split(" ");
        String outputPark= parking.Park(perintahpark[1],perintahpark[2]);
        assertEquals("Allocated slot number: 1",outputPark);
    }
    @Test
    void parkTidakBisa() {
        String input= "create_parking_lot 1";
        String[] perintah= input.split(" ");
        parking_lot parking= new parking_lot();
        String output= parking.initParking(Integer.parseInt(perintah[1]));
        assertEquals("Created a parking lot with 1 slots",output);
        String inputPark1= "park KA-01-HH-1234 White";
        String inputPark2= "park KA-01-HH-2222 White";
        String[] perintahpark= inputPark1.split(" ");
        String[] perintahpark2= inputPark2.split(" ");
        String outputPark1= parking.Park(perintahpark[1],perintahpark[2]);
        String outputPark2= parking.Park(perintahpark[1],perintahpark[2]);
        assertEquals("Allocated slot number: 1",outputPark1);
        assertEquals("Sorry, parking lot is full",outputPark2);
    }

    @Test
    void tinggalkan() {
        String input= "create_parking_lot 6";
        String[] perintah= input.split(" ");
        parking_lot parking= new parking_lot();
        String output= parking.initParking(Integer.parseInt(perintah[1]));
        assertEquals("Created a parking lot with 6 slots",output);
        String inputPark= "park KA-01-HH-1234 White";
        String[] perintahpark= inputPark.split(" ");
        String outputPark= parking.Park(perintahpark[1],perintahpark[2]);
        assertEquals("Allocated slot number: 1",outputPark);
        String inputLeft= "leave 1";
        String[] perintahLeft= inputLeft.split(" ");
        String outputLeft= parking.Tinggalkan(Integer.parseInt(perintahLeft[1]));
        assertEquals("Slot number "+perintahLeft[1]+" is free", outputLeft);
    }

    @Test
    void cariRegistNumColorFound() {
        String input= "create_parking_lot 6";
        String[] perintah= input.split(" ");
        parking_lot parking= new parking_lot();
        String output= parking.initParking(Integer.parseInt(perintah[1]));
        assertEquals("Created a parking lot with 6 slots",output);
        String inputPark= "park KA-01-HH-1234 White";
        String[] perintahpark= inputPark.split(" ");
        String outputPark= parking.Park(perintahpark[1],perintahpark[2]);
        assertEquals("Allocated slot number: 1",outputPark);
        String inputCari="registration_numbers_for_cars_with_colour White";
        String[] perintahCari= inputCari.split(" ");
        String outputCari= parking.cariRegistNumColor(perintahCari[1]);
        assertEquals("KA-01-HH-1234", outputCari);

    }
    @Test
    void cariRegistNumColorNotFound() {
        String input= "create_parking_lot 6";
        String[] perintah= input.split(" ");
        parking_lot parking= new parking_lot();
        String output= parking.initParking(Integer.parseInt(perintah[1]));
        assertEquals("Created a parking lot with 6 slots",output);
        String inputPark= "park KA-01-HH-1234 White";
        String[] perintahpark= inputPark.split(" ");
        String outputPark= parking.Park(perintahpark[1],perintahpark[2]);
        assertEquals("Allocated slot number: 1",outputPark);
        String inputCari="registration_numbers_for_cars_with_colour Black";
        String[] perintahCari= inputCari.split(" ");
        String outputCari= parking.cariRegistNumColor(perintahCari[1]);
        assertEquals("Not found", outputCari);

    }

    @Test
    void cariSlotDenganColorFound() {
        String input= "create_parking_lot 6";
        String[] perintah= input.split(" ");
        parking_lot parking= new parking_lot();
        String output= parking.initParking(Integer.parseInt(perintah[1]));
        assertEquals("Created a parking lot with 6 slots",output);
        String inputPark= "park KA-01-HH-1234 White";
        String[] perintahpark= inputPark.split(" ");
        String outputPark= parking.Park(perintahpark[1],perintahpark[2]);
        assertEquals("Allocated slot number: 1",outputPark);
        String inputCari="slot_numbers_for_cars_with_colour White";
        String[] perintahCari= inputCari.split(" ");
        String outputCari= parking.cariSlotDenganColor(perintahCari[1]);
        assertEquals("1", outputCari);
    }

    @Test
    void cariSlotDenganColorNotFound() {
        String input= "create_parking_lot 6";
        String[] perintah= input.split(" ");
        parking_lot parking= new parking_lot();
        String output= parking.initParking(Integer.parseInt(perintah[1]));
        assertEquals("Created a parking lot with 6 slots",output);
        String inputPark= "park KA-01-HH-1234 White";
        String[] perintahpark= inputPark.split(" ");
        String outputPark= parking.Park(perintahpark[1],perintahpark[2]);
        assertEquals("Allocated slot number: 1",outputPark);
        String inputCari="slot_numbers_for_cars_with_colour Black";
        String[] perintahCari= inputCari.split(" ");
        String outputCari= parking.cariSlotDenganColor(perintahCari[1]);
        assertEquals("Not found", outputCari);
    }

    @Test
    void slotForRegisNumFound() {
        String input= "create_parking_lot 6";
        String[] perintah= input.split(" ");
        parking_lot parking= new parking_lot();
        String output= parking.initParking(Integer.parseInt(perintah[1]));
        assertEquals("Created a parking lot with 6 slots",output);
        String inputPark= "park KA-01-HH-1234 White";
        String[] perintahpark= inputPark.split(" ");
        String outputPark= parking.Park(perintahpark[1],perintahpark[2]);
        assertEquals("Allocated slot number: 1",outputPark);
        String inputCari="slot_number_for_registration_number KA-01-HH-1234";
        String[] perintahCari= inputCari.split(" ");
        String outputCari= parking.SlotForRegisNum(perintahCari[1]);
        assertEquals("1", outputCari);
    }
    @Test
    void slotForRegisNumNotFound() {
        String input= "create_parking_lot 6";
        String[] perintah= input.split(" ");
        parking_lot parking= new parking_lot();
        String output= parking.initParking(Integer.parseInt(perintah[1]));
        assertEquals("Created a parking lot with 6 slots",output);
        String inputPark= "park KA-01-HH-1234 White";
        String[] perintahpark= inputPark.split(" ");
        String outputPark= parking.Park(perintahpark[1],perintahpark[2]);
        assertEquals("Allocated slot number: 1",outputPark);
        String inputCari="slot_number_for_registration_number KA-01-HH-3214";
        String[] perintahCari= inputCari.split(" ");
        String outputCari= parking.SlotForRegisNum(perintahCari[1]);
        assertEquals("Not found", outputCari);
    }

    @Test
    void checkStats() {
        String input= "create_parking_lot 6";
        String[] perintah= input.split(" ");
        parking_lot parking= new parking_lot();
        String output= parking.initParking(Integer.parseInt(perintah[1]));
        assertEquals("Created a parking lot with 6 slots",output);
        String inputPark= "park KA-01-HH-1234 White";
        String[] perintahpark= inputPark.split(" ");
        String outputPark= parking.Park(perintahpark[1],perintahpark[2]);
        assertEquals("Allocated slot number: 1",outputPark);
        String cekStatus="status";
        String outputStatus= parking.CheckStats();
        assertEquals("Slot No. Registration No Colour\n" +
                "1 KA-01-HH-1234 White",outputStatus);
    }

}